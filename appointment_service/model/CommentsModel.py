from django.db import models
from datetime import datetime

class Comments(models.Model):

    user_id       =    models.IntegerField(blank=True)
    comments      =    models.TextField(blank=False)
    created_at    =    models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = "comments"