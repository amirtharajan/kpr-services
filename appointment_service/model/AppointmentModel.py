from django.db import models

class Appointment(models.Model):

    login_id            =    models.IntegerField(null=False, blank=False)
    appointment_date    =    models.DateField(blank=False)
    requested_at        =    models.DateField(auto_now_add=True)
    subject             =    models.CharField(max_length=255, null=False, default='')
    status              =    models.CharField(max_length=50, blank=False, default='waiting')
    details             =    models.CharField(max_length = 255, null=True)

    objects = models.Manager()
    class Meta:
        db_table = "appointments"
