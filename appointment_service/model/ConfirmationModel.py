from django.db import models
from datetime import datetime
from appointment_service.model.AppointmentModel import Appointment

class Confirmation(models.Model):

    appointment                 =   models.ForeignKey(Appointment, on_delete=models.CASCADE, unique=True)
    appointment_confirmed_date  =   models.DateField(blank=False)
    appointment_start_time      =   models.TimeField(blank=False)
    appointment_duration        =   models.CharField(max_length=3, blank=False)
    confirmed_at                =   models.DateTimeField(auto_now_add=True)

    objects = models.Manager()
    class Meta:
        db_table = "confirmations"
