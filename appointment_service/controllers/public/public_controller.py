from django.contrib import messages
from datetime import datetime

from django.shortcuts import render, redirect

from user_service.helpers import verify_kpr_ticket
from user_service.email.email import send_email

from user_service.controllers.public.public_controller import logout

from pluck import pluck

from appointment_service.model.AppointmentModel import Appointment
from appointment_service.model.ConfirmationModel import Confirmation

def my_appointments(request):
    if 'kpr_ticket' not in request.COOKIES:
        messages.warning(request, 'Please Login')
        return redirect('login')

    data = verify_kpr_ticket(request.COOKIES['kpr_ticket'])
    if not data:
        return logout(request)

    if 'cancel_id' in request.POST:
        appointment = Appointment.objects.filter(id=request.POST['cancel_id']).first()
        if appointment.status != 'confirmed':
            appointment.status = 'canceled_by_user'
            appointment.save()
            messages.success(request, 'Appoinment canceled')
            return render(request, 'my_appointments.html')

        messages.error(request, 'Sorry, Your appointmet is confirmed. Contact Admin')
        return render(request, 'my_appointments.html')

    if 'date' in request.GET:
        requested_date = request.GET['date']
        requested_date = str(requested_date).split(' ')
        appointments = Appointment.objects.filter(appointment_date=requested_date[0], login_id=data['id'])
        appointment_ids = pluck(appointments, 'id')
        confirmed_appoiments = Confirmation.objects.filter(appointment__in=appointment_ids)
        all_details = []
        if confirmed_appoiments:
            for data in appointments:
                for confirmed in confirmed_appoiments:
                    if data.id == confirmed.appointment_id:
                        detail = {
                            'appointment_id': data.id,
                            'confirmed_id' : confirmed.id,
                            'status': data.status,
                            'appointment_date' : data.appointment_date,
                            'appointment_confirmed_date': confirmed.appointment_confirmed_date,
                            'appointment_start_time': confirmed.appointment_start_time,
                            'appointment_duration': confirmed.appointment_duration,
                            'confirmed_at' : confirmed.confirmed_at,
                        }
                        all_details.append(detail)
        
        all_request = []
        for data in appointments:
            if data.status == 'waiting':
                detail = {
                    'appointment_id': data.id,
                    'appointment_date' : data.appointment_date,
                    'requested_at': data.requested_at,
                    'subject' : data.subject,
                    'status': data.status,
                    'details': data.details,
                }
                all_request.append(detail)
        return render(request, 'my_appointments.html', {'appointment_details':all_details, 'request_details':all_request, 'requested_date':requested_date[0]})
    return render(request, 'my_appointments.html')

def appointment(request):
    if 'kpr_ticket' not in request.COOKIES:
        messages.warning(request, 'Please Login')
        return redirect('login')

    data = verify_kpr_ticket(request.COOKIES['kpr_ticket'])
    if not data:
        return logout(request)

    if request.method == 'POST':
        new = {
            'appointment_date'  :   request.POST['appointment_date'],
            'subject'           :   request.POST['subject'],
            'details'           :   request.POST['details'],
        }
        today = str(datetime.now())
        today = today.split(' ')
        if today[0] <= str(new['appointment_date']):
            new_appointment = Appointment(
                login_id = data['id'],
                appointment_date = new['appointment_date'],
                subject = new['subject'],
                details = new['details'],
            )
            new_appointment.save()
            messages.success(request, 'Apppointment registred, We successfully send the mail acknowledgement')
            subject = 'Appointment request to KPRIEnT Principal'
            content = '''Dear {}, \n
                        Appointment successfully registred\n
                        Once the Appointment is confirmed, we will send the Confirmation mail
                        Your Appointment request date: {}\n\n
                        Thanks & Regards,
                        Team KPRIEnT & KPRCAnS'''.format(data['first_name'], request.POST['appointment_date'])
            send_email(data['username'], subject, content)
            return redirect('appointment')

        messages.error(request, 'Check your request date')
        return render(request, 'appointment.html')

    return render(request, 'appointment.html')
