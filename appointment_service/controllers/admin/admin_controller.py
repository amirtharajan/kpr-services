from django.contrib import messages
from django.shortcuts import render, redirect
import datetime

from user_service.email.email import send_email
from appointment_service.helpers import check_user
from pluck import pluck

from appointment_service.model.AppointmentModel import Appointment
from appointment_service.model.ConfirmationModel import Confirmation

from user_service.model.UserModel import User
from user_service.models import Login

today = datetime.datetime.now()


def list_appointments(request):
    if 'kpr_ticket' not in request.COOKIES:
        messages.warning(request, 'Please Login')
        return redirect('login')

    user = check_user(request.COOKIES['kpr_ticket'])
    if not user:
        messages.warning(request, 'You dont have any admin privilege')
        return redirect('appointment')

    requested_date = None
    if request.method == 'GET':
        if 'd' in request.GET and request.GET['d']:
            requested_date = request.GET['d']
            date = str(requested_date).split(' ')
            confirmation_details = Confirmation.objects.filter(appointment_confirmed_date=date[0])

        elif 'all_confirmed' in request.GET:
            confirmation_details = Confirmation.objects.filter(appointment_confirmed_date__gte=today).order_by('appointment_confirmed_date')

        elif 'delete_confirmed_appointment' in request.GET:
            confirmation_data = Confirmation.objects.filter(id=request.GET['delete_confirmed_appointment']).first()
            appointment_data = Appointment.objects.filter(id=confirmation_data.appointment_id).first()
            user = User.objects.filter(login_id=appointment_data.login_id).first()
            appointment_data.status = 'waiting'
            confirmation_data.delete()
            appointment_data.save()
            subject = 'Appointment Canceled with KPRIEnT Principal'
            content = '''Dear, {} {}\n
                        Your appointment request has been canceled because the Principal is not available\n
                        We will give another time slot as soon as possible\n\n
                        Thanks & Regards,
                        Team KPRIEnT & KPRCAnS'''.format(user.first_name, user.last_name)
            send_email(user.college_email, subject, content)
            messages.success(request, 'Appointment canceled')
            return redirect('appointments')

        else:
            confirmation_details = Confirmation.objects.filter(appointment_confirmed_date__gte=today).order_by('appointment_confirmed_date')

    request_ids = pluck(confirmation_details, 'appointment_id')
    request_details = Appointment.objects.filter(id__in=request_ids)
    login_ids = pluck(request_details, 'login_id')
    user_details = User.objects.filter(login_id__in=login_ids)

    appointment_confirmation_details = []
    for request_detail, confirmation_detail in zip (request_details, confirmation_details):
        for user_detail in user_details:
            if request_detail.login_id == user_detail.login_id:

                data = {
                    'appointment_request_id': request_detail.id,
                    'appointment_confirmation_id': confirmation_detail.id,
                    'appointment_confirmed_date': confirmation_detail.appointment_confirmed_date,
                    'appointment_confirmed_time': confirmation_detail.appointment_start_time,
                    'appointment_confirmed_duration': confirmation_detail.appointment_duration,
                    'confirmed_at' : confirmation_detail.confirmed_at,
                    'requested_at' : request_detail.requested_at,
                    'subject' : request_detail.subject,
                    'details' : request_detail.details,
                    'status': request_detail.status,
                    'login_id' : user_detail.login_id,
                    'first_name' : user_detail.first_name,
                    'last_name' : user_detail.last_name,
                    'profile_photo' : user_detail.profile_photo,
                    'gender' : user_detail.gender,
                    'college_email' : user_detail.college_email,
                    'phone' : user_detail.phone,
                    'department' : user_detail.department,
                    'designation' : user_detail.designation,
                    'year' : user_detail.year,
                    'section' : user_detail.section
                }
                appointment_confirmation_details.append(data)
    if not appointment_confirmation_details:
        messages.info(request, 'No Appointments on {}'.format(requested_date))

    total = len(appointment_confirmation_details)
    return render(request, 'appointment_confirm.html', {'user_details': appointment_confirmation_details, 'total': total, 'dd':requested_date})

def list_request(request):
    if 'kpr_ticket' not in request.COOKIES:
        messages.warning(request, 'Please Login')
        return redirect('login')

    user = check_user(request.COOKIES['kpr_ticket'])
    if not user:
        messages.warning(request, 'You dont have any admin privilege')
        return redirect('appointment')
    d = today.now()
    today_date = str(d).split(' ')
    time_now = today_date[1].split(':')
    time_now = time_now[0] + ':' + time_now[1]

    if request.method == 'POST':
        
        if 'confirmed_date' in request.POST:
            appointment_data = Appointment.objects.filter(id=request.POST['request_id']).first()
            if not (request.POST['start_time'] < time_now and str(request.POST['confirmed_date']) < today_date[0]):
                user = User.objects.filter(login_id=appointment_data.login_id).first()
                appointment_data.status = 'confirmed'
                appointment_data.save()
                new_confirmed_appointment = Confirmation(
                    appointment_confirmed_date = request.POST['confirmed_date'],
                    appointment_start_time = request.POST['start_time'],
                    appointment_duration = request.POST['duration'],
                    confirmed_at = today.now(),
                    appointment_id = request.POST['request_id'],
                )
                new_confirmed_appointment.save()

                subject = 'Appointment Confirmed with KPRIEnT Principal'
                content = '''Dear, {} {}\n
                            Your Appointment request is Confirmed\n
                            Appointment Date :     \t{}
                            Appointment Time :     \t{}
                            Appointment Duration : \t{} minutes\n\n
                            Thanks & Regards,
                            Team KPRIEnT & KPRCAnS'''.format(user.first_name, user.last_name, request.POST['confirmed_date'], request.POST['start_time'], request.POST['duration'])
                send_email(user.college_email, subject, content)

                messages.success(request, 'Appointment Confirmed, The mail notification is successfully send to {}'.format(user.first_name))
                return redirect('appointment_admin')
            else:
                messages.warning(request, 'Check appointment confirmation time')
                d = appointment_data.appointment_date
                return render(request, 'appointment_admin.html')

        elif 'delete_id' in request.POST:
            req = Appointment.objects.filter(id=request.POST['delete_id']).first()
            user = Login.objects.filter(id=req.login_id).first()
            user = User.objects.filter(login_id=user.id).first()
            if req:
                d =  req.appointment_date
                req.status = 'rejected'
                req.save()
                messages.info(request, 'ID {}, Rejected. The mail notifications is successfully send to the user'.format(request.POST['delete_id']))
                subject = 'Appointment Canceled with KPRIEnT Principal'
                content = '''Dear, {} {}\n
                            Your Appointment request is canceled\n\n
                            Thanks & Regards,
                            Team KPRIEnT & KPRCAnS'''.format(user.first_name, user.last_name)
                send_email(user.college_email, subject, content)
                messages.success(request, 'Appointment Canceled, The mail notification is successfully send to {}'.format(user.first_name))
                return redirect('appointment_admin')
    d = None
    if 'd' in request.GET:
        d = request.GET['d']
        if not d:
            messages.warning(request, 'Please select date')
            return redirect('appointment_admin')

        date = str(d).split(' ')
        d = date[0]
        request_details = Appointment.objects.filter(appointment_date=d, status='waiting').order_by( 'appointment_date')

    elif 'all_request' in request.GET:
        request_details = Appointment.objects.filter(appointment_date__gte=today, status='waiting').order_by( 'appointment_date')

    else:
        request_details = Appointment.objects.filter(appointment_date__gte=today, status='waiting').order_by( 'appointment_date')

    login_ids = pluck(request_details, 'login_id')
    user_details = User.objects.filter(login_id__in=login_ids)
    appointment_request_details = []
    for no, request_detail in enumerate (request_details):
        for user_detail in user_details:
            if request_detail.login_id == user_detail.login_id:

                data = {
                    'no': no+1,
                    'id': request_detail.id,
                    'subject': request_detail.subject,
                    'details': request_detail.details,
                    'status': request_detail.status,
                    'appointment_required_date':request_detail.appointment_date,
                    'requested_at': request_detail.requested_at,
                    'login_id' : request_detail.login_id,
                    'first_name' : user_detail.first_name,
                    'last_name': user_detail.last_name,
                    'profile_photo': user_detail.profile_photo,
                    'gender' : user_detail.gender,
                    'college_email' : user_detail.college_email,
                    'phone' : user_detail.phone,
                    'department' : user_detail.department,
                    'designation' : user_detail.designation,
                    'year' : user_detail.year,
                    'section' : user_detail.section
                }
                appointment_request_details.append(data)
    if not appointment_request_details:
        messages.info(request, 'No request on {}'.format(d))
        return render(request, 'appointment_admin.html', {'today':today_date[0], 'd':d, 'time_now':time_now})

    total = len(appointment_request_details)
    return render(request, 'appointment_admin.html', {'today':today_date[0], 'time_now':time_now, 'request_details':appointment_request_details, 'total':total})


