
from django.urls import path
from django.conf import settings
from appointment_service.controllers.public.public_controller import (
    appointment,
    my_appointments,
)

from appointment_service.controllers.admin.admin_controller import (
    list_request,
    list_appointments
)


urlpatterns = [
    path('', appointment, name='appointment'),
    path('admin/', list_request, name='appointment_admin'),
    path('admin/appointments', list_appointments, name='appointments'),
    path('my', my_appointments,  name='my_appointments'),
]
