from user_service.helpers import verify_kpr_ticket

def check_user(kpr_ticket):
    user = verify_kpr_ticket(kpr_ticket)
    if not user:
        return False

    if user['role'] == 'appointment_admin' or user['role'] == 'super_admin':
        return user
    return False