from django.urls import path

from user_service.controllers.admin.admin_controller import (
    user_admin,
    list_user,
)

urlpatterns = [
    path('', user_admin, name='user_admin'),
    path('user/list/', list_user, name='list_user'),
]
