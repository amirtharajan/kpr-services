
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from user_service.controllers.admin.admin_controller import (
    user_admin,
    list_user,
)
from user_service.controllers.public.public_controller import (
    login,
    services,
    register,
    logout,
    forget_password,
    reset_password,
    get_my_profile,
    update_profile,
    delete_account,
    validate_otp,
    resend_otp,
    forget_password_otp,
    # check
)

urlpatterns = [
    path('', login, name='login'),
    # path('check/', check),
    path('services/', services, name='services'),
    path('register/', register, name='register'),
    path('logout/', logout, name='logout'),
    path('forget/password/', forget_password, name='forget'),
    path('reset/password/', reset_password, name='reset'),
    path('profile/update/', update_profile, name='update'),
    path('profile/', get_my_profile, name='profile'),
    path('profile/delete', delete_account, name='delete_account'),
    path('validate-otp/', validate_otp, name='otp'),
    path('resend-otp/', resend_otp, name='resend_otp'),
    path('forget_password_otp/', forget_password_otp, name='forget_password_otp'),
]

urlpatterns = urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)