from django.db import models
from datetime import datetime
from user_service.models import Login
from django.contrib.auth.hashers import make_password

class User(models.Model):

    GENDER = [
	    ('', u''),
        ('Male', u'Male'),
   	    ('Female', u'Female'),
        ('Other', u'Other')
	]

    DEPARTMENT = [
        ('', u''),
        ('Computer Science & Engineering' , u'Computer Science & Engineering'),
        ('Electrical & Communication Engineering' , u'Electrical & Communication Engineering'),
        ('Electrical & Electronics Engineering' , u'Electrical & Electronics Engineering'),
        ('Civil Engineering' , u'Civil Engineering'),
        ('Mechanical Engineering' , u'Mechanical Engineering'),
        ('Biomedical Engineering' , u'Biomedical Engineering'),
        ('Chemical Engineering' , u'Chemical Engineering'),
        ('Physical Education' , u'Physical Education'),
        ('Management' , u'Management'),
        ('Food & Beverage' , u'Food & Beverage'),
        ('Transport' , u'Transport'),
        ('Human Resource' , u'Human Resource'),
        ('Research Division' , u'Research Division'),
        ('Other' , u'Other')
    ]

    DESIGNATION = [
        ('', u''),
        ('Chairman' , u'Chairman'),
        ('Manageing Director' , u'Manageing Director'),
        ('Trustee' , u'Trustee'),
        ('Chief Executive Officer' , u'Chief Executive Officer'),
        ('Principal' , u'Principal'),
        ('General Manager' , u'General Manager'),
        ('Research & Head' , u'Research & Head'),
        ('College Admin' , u'College Admin'),
        ('Human Resource Manager' , u'Human Resource Manager'),
        ('Developer', u'Developer'),

        ('Head Of the Department' , u'Head Of the Department'),
        ('Professor' , u'Professor'),
        ('Associate Professor' , u'Associate Professor'),
        ('Assistant Professor' , u'Assistant Professor'),
        ('Lab Assistant' , u'Lab Assistant'),
        ('Department Assistant' , u'Department Assistant'),
        ('Student' , u'Student'),
        ('Office Staff' , u'Office Staff'),
        ('Other' , u'Other')
    ]

    login                       =    models.ForeignKey(Login, on_delete=models.CASCADE, unique=True, related_name='login_data')
    first_name                  =    models.CharField(max_length = 255, default='')
    last_name                   =    models.CharField(max_length = 255)
    profile_photo               =    models.FileField(upload_to='images/profile_photos', default='images/no_image1.png')
    gender                      =    models.CharField(choices=GENDER, max_length=10, null=False, default='')
    college_email               =    models.EmailField(max_length=255, null=False, unique=True, default='')
    phone                       =    models.CharField(max_length=30, default='')
    department                  =    models.CharField(choices=DEPARTMENT, max_length=128, default='Other')
    designation                 =    models.CharField(choices=DESIGNATION, max_length=128, default='')
    year                        =    models.CharField(max_length=1, default='1')
    section                     =    models.CharField(max_length=1, default='A')
    created_at                  =    models.DateTimeField(auto_now_add=True)
    updated_at                  =    models.DateTimeField(auto_now=True)

    objects = models.Manager()

    class Meta:
        db_table = "kpr_users"
