from django.db import models
from datetime import datetime

class ContactRequest(models.Model):

    name        =   models.IntegerField(blank=True)
    email       =   models.CharField(max_length=100, blank=True)
    phone       =   models.CharField(max_length=13, blank=True)
    details     =   models.TextField(blank=True)
    created_at  =   models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = "contact_request"