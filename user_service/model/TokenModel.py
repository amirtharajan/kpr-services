from django.db import models
from user_service.models import Login

class Token(models.Model):

    login   =     models.OneToOneField(Login, on_delete=models.CASCADE, unique=True, default='')
    kpr_ticket   =     models.CharField(max_length=1000, blank=False, default='not defined')

    objects = models.Manager()

    class Meta:
        db_table = "tokens"