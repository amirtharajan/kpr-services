from django.conf import settings
from django.contrib.auth.hashers import make_password

from datetime import datetime

from user_service.models import Login

import jwt
from jwt import DecodeError

from io import BytesIO
from PIL import Image
from django.core.files.uploadedfile import InMemoryUploadedFile

SALT = settings.SALT
HASHER = settings.HASHER

JWT_SECRET_KEY = settings.JWT_SECRET_KEY
JWT_ALGORITHM = settings.JWT_ALGORITHM

def make_hash(user_password):
    hash_password = make_password(user_password, salt=SALT, hasher=HASHER)
    return hash_password

def generate_kpr_ticket(data):
    if data:
        kpr_ticket = jwt.encode(data, JWT_SECRET_KEY, JWT_ALGORITHM).decode('utf-8')
        return kpr_ticket

    return None

def verify_kpr_ticket(kpr_ticket):
    if kpr_ticket:
        try:
            data = jwt.decode(kpr_ticket, JWT_SECRET_KEY, JWT_ALGORITHM)
            expire_date = datetime.strptime(data['expire'], '%Y-%m-%d %H:%M:%S.%f')
            if datetime.now() >= expire_date:
                return None
            check_role = Login.objects.get(id=data['id'])
            if not check_role:
                return None
            if check_role.role == 'blocked_user':
                return None
            return data
        except jwt.DecodeError:
            return None

    return None

IMAGE_FILE_TYPES = ['jpg', 'jpeg', 'JPG', 'JPEG']
def image_handeller(image, name):
    check_file_type = str(image).split('.')
    if check_file_type[-1] not in IMAGE_FILE_TYPES:
        return False

    i = Image.open(image)
    thumb_io = BytesIO()
    i.save(thumb_io, format='JPEG', quality=20)
    image_file = InMemoryUploadedFile(thumb_io, None, name + '.jpeg', None, thumb_io.tell(), None)
    return image_file
