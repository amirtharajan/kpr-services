from django.contrib.auth.models import auth
from django.contrib.auth import authenticate
from datetime import datetime, timedelta
from random import randint
from django.http import HttpResponse

from django.contrib import messages

from django.shortcuts import render, redirect, get_object_or_404

from user_service.helpers import make_hash, generate_kpr_ticket, verify_kpr_ticket, image_handeller

from user_service.models import Login
from user_service.model.UserModel import User
from user_service.model.TokenModel import Token
from user_service.model.LoginHistoryModel import LoginHistory

from jwt import DecodeError

from user_service.email.email import send_email

IMAGE_FILE_TYPES = ['png', 'jpg', 'jpeg']

# def check(request):
#     data = verify_kpr_ticket(request.GET['id'])
#     if data:
#         return HttpResponse(True)
#     return HttpResponse(False)

def register(request):
    if request.user.is_authenticated:
            messages.info(request, 'You already registred')
            return redirect('services')

    if request.method == 'POST':
        if 'profile_photo' in request.FILES:
            name = str(request.POST['name']).split('.')
            profile_photo = image_handeller(request.FILES['profile_photo'], name[0])
        else:
            profile_photo = 'images/no_image1.png'

        data = {
            'first_name': request.POST['first_name'].title(),
            'last_name': request.POST['last_name'].title(),
            'gender': request.POST['gender'].title(),
            'profile_photo' : profile_photo,
            'college_email': request.POST['college_email'],
            'password': request.POST['password'],
            'confirm_password': request.POST['confirm_password'],
            'phone': request.POST['phone'],
            'department': request.POST['department'].title(),
            'designation': request.POST['designation'].title(),
            'year': request.POST['year'],
            'section': request.POST['section'].title()
        }

        if not data['first_name']:
            messages.error(request, 'FIRSTNAME_IS_REQUIRED')
            return render(request, 'register.html', {'form':data})

        if not data['gender']:
            messages.error(request, 'GENDER_IS_REQUIRED')
            return render(request, 'register.html', {'form':data})

        if not data['college_email']:
            messages.error(request, 'COLLEGE_EMAIL_REQUIRED')
            return render(request, 'register.html', {'form':data})

        college_email = data['college_email'].split('@')
        if 'kpriet.ac.in' != college_email[1] and 'kprcas.ac.in' != college_email[1]:
            messages.error(request, 'COLLEGE_EMAIL_ONLY_ALLOWED_TO_REGISTER')
            return render(request, 'register.html', {'form':data})

        if not data['phone']:
            messages.error(request, 'PHONE_NUMBER_IS_REQUIRED')
            return render(request, 'register.html', {'form':data})

        check_phone = User.objects.filter(phone=data['phone'])
        if check_phone:
            messages.error(request, 'PHONE_NUMBER_ALREADY_EXISTS')
            return render(request, 'register.html', {'form': data})

        check_email = Login.objects.filter(username=data['college_email'])
        if check_email:
            messages.error(request, 'COLLEGE_EMAIL_ALREADY_EXISTS')
            return render(request, 'register.html', {'form':data})

        if data['password'] != data['confirm_password']:
            messages.error(request, 'PASSWORD_MISMATCH, TRY_AGAIN')
            return render(request, 'register.html', {'form':data})

        if not data['designation']:
            messages.error(request, 'Designation is required')
            return render(request, 'register.html', {'form':data})

        if data['designation'] != 'Student' :
            validate_email = data['college_email'].split('@')
            prefix = validate_email[0]
            if prefix[0].isdigit() or prefix[1].isdigit() or prefix[-1].isdigit() or prefix[-2].isdigit():
                messages.error(request, 'Check your COLLEGE EMAIL or DESIGNATION')
                return render(request, 'register.html', {'form': data})

        if data['designation'] == 'Student' :
            validate_email = data['college_email'].split('@')
            prefix = validate_email[0]
            if not prefix[0].isdigit() or not prefix[1].isdigit() or not prefix[-1].isdigit() or not prefix[-2].isdigit():
                messages.error(request, 'Check your COLLEGE EMAIL or DESIGNATION')
                return render(request, 'register.html', {'form': data})

        if data['designation'] == 'Student':
            if not data['year'] or not data['section']:
                messages.error(request, 'STUDENT must give YEAR and SECTION details')
                return render(request, 'register.html', {'form': data})

        validate_section = data['section'].title()
        sections = ['A','B','C','D']
        if validate_section not in sections:
            messages.error(request, 'SECTION is not valid')
            return render(request, 'register.html', {'form': data})

        if not profile_photo:
            messages.error(request, 'JPG or JPEG format only allowed for the Profile')
            return render(request, 'register.html', {'form': data})

        role = 'user'
        designation = [
            'Head Of the Department',
            'Professor',
            'Associate Professor',
            'Assistant Professor',
            'Lab Assistant',
            'Department Assistant',
            'Office Staff'
        ]
        if data['designation'] == 'Student':
            role = 'student'
        elif data['designation'] in designation:
            role = 'staff'

        password = make_hash(data['password'])

        new_login = Login(
            username = data['college_email'],
            password = password,
            role=role,
            active=False
        )
        new_login.save()

        id = Login.objects.only('id').get(username=new_login.username).id
        new_user = User(
            login_id = id,
            first_name = data['first_name'],
            last_name = data['last_name'],
            profile_photo = profile_photo,
            gender = data['gender'],
            college_email = data['college_email'],
            phone = data['phone'],
            department = data['department'],
            designation = data['designation'],
            year = data['year'],
            section = data['section']
        )

        new_user.save()

        messages.success(request, 'Mr/Mrs/Miss {}, YOUR_ACCOUNT_HAS_BEEN_CREATED_AND_OTP_SUCCESSFULLY_SEND_TO_YOUR_MAIL'.format(data['first_name']))
        return generate_otp(data['first_name'], data['last_name'], data['college_email'])

    return render(request, 'register.html')

def generate_otp(first_name, last_name, email):
        randam_number = randint(1000, 9999)
        otp = str(randam_number) + ',' + email
        subject = 'OTP - KPR USER-SERVICE'
        content = '''Dear, {} {}\n
                    Account successfully created.\n
                    Enthe the following OTP to activate your account
                    Your OTP : {}'''.format(first_name, last_name, randam_number)
        send_email(email, subject, content)
        response = redirect('otp')
        response.set_cookie('otp', otp)
        return response

def validate_otp(request):
    if request.method == 'POST':
        if 'otp' in request.COOKIES:
            otp = request.COOKIES['otp'].split(',')
            if request.POST['otp'] == otp[0]:
                login = Login.objects.filter(username=otp[1]).first()
                login.active = True
                login.save()
                messages.success(request, 'OTP VERIFIED, NOW YOU CAN LOGIN')
                response = redirect('login')
                response.delete_cookie('otp')
                return response

            messages.error(request, 'Invalid OTP')
            return redirect('otp')

        messages.success(request, 'OTP Not found')
        return redirect('register')

    return render(request, 'otp.html')

def resend_otp(request):
    if request.method == 'POST':
        user = User.objects.filter(college_email=request.POST['college_email']).first()
        if not user:
            return redirect('login')

        otp = request.COOKIES['otp'].split(',')
        subject = 'OTP - KPR USER-SERVICE'
        content = '''Dear, {} {}\n
                    Your OTP : {}'''.format(user.first_name, user.last_name, otp[0])
        send_email(user.college_email, subject, content)
        return redirect('otp')

    return render(request, 'resend_otp.html')

def login(request):
    if request.user.is_authenticated:
        kpr_ticket = Token.objects.filter(login_id=request.user.id).first()
        if kpr_ticket:
            response = redirect('services')
            response.set_cookie('kpr_ticket', kpr_ticket.kpr_ticket)
            return response

    elif request.method == 'POST':
        check_user = Login.objects.filter(username=request.POST['username']).first()
        if not check_user:
            messages.warning(request, 'Sorry, Username is not found. If you are a new user, Please REGISTER. Else login with correct Username')
            return render(request, 'login.html', {'username':request.POST['username']})

        if check_user.active == False:
            messages.error(request, 'Your account is not verified, Please enter the OTP or RESEND the OTP')
            return redirect('otp')

        if check_user.role == 'blocked_user':
            messages.error(request, 'Your account has been blocked, Contact User Admin for More information')
            return render(request, 'login.html')

        login = auth.authenticate(username=request.POST['username'], password=request.POST['password'])
        if not login:
            messages.warning(request, 'Sorry, Given password is wrong')
            return render(request, 'login.html', {'username':request.POST['username']})

        auth.login(request,login)
        user = get_object_or_404(User, login_id=login.id)
        data = {
            'id' : login.id,
            'username': login.username,
            'first_name': user.first_name,
            'role': login.role,
            'expire':str(datetime.now() + timedelta(days=30))
            }

        login.save()

        kpr_ticket = generate_kpr_ticket(data)
        Token.objects.update_or_create(login_id=login.id, defaults={'kpr_ticket': kpr_ticket})

        LoginHistory(login_id=request.user.id).save()
        messages.success(request, 'Welcome {}'.format(user.first_name))
        response = redirect('services')
        response.set_cookie('kpr_ticket', kpr_ticket)
        return response

    response = render(request, 'login.html')
    response.delete_cookie('kpr_ticket')
    return response

def services(request):
    if 'kpr_ticket' not in request.COOKIES or not request.user.is_authenticated:
        return redirect('login')
    return render(request, 'services.html')

def logout(request):
    auth.logout(request)
    response = redirect('login')
    response.delete_cookie('kpr_ticket')
    return response

def forget_password(request):
    if 'kpr_ticket' in request.COOKIES or not request.user.is_authenticated:
        return redirect('services')

    if request.method == 'POST':
        data = {
            'college_email': request.POST['college_email'],
            'password': request.POST['new_password'],
            'confirm_password': request.POST['confirm_password']
        }

        if data['password'] != data['confirm_password']:
            messages.error(request, 'PASSWORD_MISMATCH')
            return render(request, 'forget_password.html')

        user = User.objects.filter(college_email=data['college_email']).first()
        if not user:
            messages.success(request, 'Email ID not found')
            return render(request, 'forget_password.html')

        password = make_hash(data['password'])
        randam_number = randint(1000, 9999)
        otp = str(randam_number) + ',' + data['college_email'] + ',' + password
        subject = 'OTP - KPR USER-SERVICE'
        content = '''Dear, {} {}\n
                    Enthe the following OTP to Recover your Password
                    Your OTP : {}'''.format(user.first_name, user.last_name, randam_number)
        send_email(data['college_email'], subject, content)
        response = redirect('forget_password_otp')
        response.set_cookie('forget_password_otp', otp)
        messages.success(request, 'OTP Successfully send to your email')
        return response
    return render(request, 'forget_password.html')

def forget_password_otp(request):
    if request.method == 'POST':
        otp = request.COOKIES['forget_password_otp'].split(',')
        if request.POST['otp'] == otp[0]:
            login = Login.objects.filter(username=otp[1]).first()
            login.password = otp[2]
            login.save()
            messages.success(request, 'OTP VERIFIED, YOUR PASSWORD RECOVERED. NOW YOU CAN LOGIN')
            response = redirect('login')
            response.delete_cookie('forget_password_otp')
            return response
        return redirect('otp')
    return render(request, 'otp.html')

def reset_password(request):
    if 'kpr_ticket' not in request.COOKIES or not request.user.is_authenticated:
        messages.warning(request, 'Please Login to Reset the password')
        return redirect('login')

    data = verify_kpr_ticket(request.COOKIES['kpr_ticket'])
    if not data:
        return logout(request)

    if request.method == 'POST':
        data = {
            'old_password': request.POST['old_password'],
            'new_password': request.POST['new_password'],
            'confirm_password': request.POST['confirm_password']
        }

        if data['new_password'] != data['confirm_password']:
            messages.error(request, 'PASSWORD_MISMATCH')
            return render(request, 'reset_password.html')

        login_user = Login.objects.filter(username=request.user).first()
        old_password = make_hash(data['old_password'])

        if old_password != login_user.password:
            messages.error(request, 'WRONG_OLD_PASSWORD')
            return render(request, 'reset_password.html')

        new_password = make_hash(data['new_password'])

        login_user.password = new_password
        login_user.save()
        messages.success(request, 'Password reset successfully, Please login')
        return logout(request)

    return render(request, 'reset_password.html')

def get_my_profile(request):
    if 'kpr_ticket' not in request.COOKIES or not request.user.is_authenticated:
        messages.warning(request, 'Please Login to Reset the password')
        return redirect('login')

    data = verify_kpr_ticket(request.COOKIES['kpr_ticket'])
    if not data:
        return logout(request)

    user_details = User.objects.filter(login_id=request.user.id).first()
    login_details = Login.objects.filter(id=request.user.id).first()

    return render(request, 'profile.html', {'user_details': user_details, 'login_details' : login_details})

def update_profile(request):
    if 'kpr_ticket' not in request.COOKIES or not request.user.is_authenticated:
        messages.warning(request, 'Please Login to Reset the password')
        return redirect('login')

    data = verify_kpr_ticket(request.COOKIES['kpr_ticket'])
    if not data:
        return logout(request)

    user_details = User.objects.filter(login_id=request.user.id).first()
    if request.method == 'POST':
        if 'profile_photo' not in request.FILES:
            profile_photo = user_details.profile_photo
        else:
            profile_photo = image_handeller(request.FILES['profile_photo'], user_details.first_name)

        if not profile_photo:
            messages.error(request, 'JPG or JPEG format only allowed for FOOD ITEM PHOTO')
            return redirect('update')

        user_details.first_name = request.POST['first_name'].title()
        user_details.last_name = request.POST['last_name'].title()
        user_details.profile_photo = profile_photo
        user_details.phone = request.POST['phone']
        user_details.department = request.POST['department'].title()
        user_details.designation = request.POST['designation'].title()
        user_details.year = request.POST['year']
        user_details.section = request.POST['section'].title()

        user_details.save()

        messages.success(request, 'SUCCESSFULLY_UPDATED')
        return redirect('services')

    return render(request,'update_profile.html', {'user_details': user_details})

def change_profile_picture(request):
    pass

def delete_account(request):
    if 'kpr_ticket' not in request.COOKIES or not request.user.is_authenticated:
        messages.warning(request, 'Please Login to Reset the password')
        return redirect('login')

    data = verify_kpr_ticket(request.COOKIES['kpr_ticket'])
    if not data:
        return logout(request)

    if not request.user.is_authenticated:
            return redirect('login')
    user = User.objects.filter(login_id=request.user.id).first()
    login = Login.objects.filter(id=request.user.id).first()
    user.delete()
    login.delete()
    messages.success(request, 'YOUR_ACCOUNT_HAS_BEEN_DELETED_SUCCESSFULLY')
    return redirect('login')
