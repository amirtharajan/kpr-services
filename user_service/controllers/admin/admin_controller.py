from django.contrib import messages
from django.shortcuts import render, redirect

from django.contrib.sessions.models import Session

from user_service.helpers import verify_kpr_ticket, make_hash
from user_service.models import Login
from user_service.model.UserModel import User
from user_service.email.email import send_email

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


def check_user(kpr_ticket):
    data = verify_kpr_ticket(kpr_ticket)
    if not data:
        return redirect('login')

    if data['role'] == 'user_admin' or data['role'] == 'super_admin' or data['role'] == 'developer':
        return data
    return False

def user_admin(request):
    if 'kpr_ticket' not in request.COOKIES or not request.user.is_authenticated:
        messages.warning(request, 'Please Login')
        return redirect('login')

    data = check_user(request.COOKIES['kpr_ticket'])
    if not data:
        messages.error(request, 'You are not admin for KPR USER SERVICE')
        return redirect('services')

    if request.method == 'POST':
        if 'get_user' in request.POST:
            login = Login.objects.filter(username=request.POST['get_user']).first()
            if not login:
                messages.error(request, 'Username "{}" Not Found'.format(request.POST['get_user']))
                return render(request, 'user_admin_dashboard.html',{'get_user':request.POST['get_user']})

            user = User.objects.filter(login_id=login.id).first()
            if data['role'] == 'user_admin' and (login.role == 'super_admin' or login.role == 'developer'):
                messages.error(request, 'You dont have a privilege to GET this {} profile details'.format(request.POST['get_user']))
                return render(request, 'user_admin_dashboard.html',{'get_user':request.POST['get_user']})

            user_detail = {
                'login_id': login.id,
                'user_id': user.id,
                'username': login.username,
                'role':login.role,
                'first_name': user.first_name,
                'last_name': user.last_name,
                'gender': user.gender,
                'college_email': user.college_email,
                'phone': user.phone,
                'department': user.department,
                'designation': user.designation,
                'year': user.year,
                'section': user.section,
                'last_login': login.last_login,
                'created_at': user.created_at,
                'updated_at': user.updated_at
            }
            return render(request, 'user_admin_dashboard.html', {'user_detail': user_detail})

        elif 'new_user' in request.POST:
            user = Login.objects.filter(username=request.POST['new_user']).first()
            if user:
                messages.warning(request, 'Username "{}" Already  Registered'.format(request.POST['new_user']))
                return render(request, 'user_admin_dashboard.html', {'new_user':request.POST['new_user']})

            if data['role'] == 'user_admin' and (request.POST['role'] == 'super_admin' or request.POST['role'] == 'developer' or request.POST['role'] == 'user_admin'):
                messages.warning(request, 'You dont have privilege to create {} role user'.format(request.POST['role']))
                return render(request, 'user_admin_dashboard.html', {
                    'new_user': request.POST['new_user'],
                    'password': request.POST['password'],
                    'role':request.POST['role'],
                })
            new_login = Login(
                username=request.POST['new_user'],
                password=make_hash(request.POST['password']),
                role=request.POST['role'],
                active=True
            )
            new_login.save()
            user = Login.objects.filter(username=request.POST['new_user']).first()
            new_user = User(college_email=user.username, login_id=user.id)
            new_user.save()
            messages.success(request, 'Username "{}" has been ADDED successfully'.format(request.POST['new_user']))
            subject = 'Welcome to KPR - SERVICES'
            content = '''Dear,\n
                        Welcome to KPR Services\n
                        USERNAME : {}
                        PASSWORD : {}\n
                        Now you can LOGIN\n\n
                        Thanks & Regards,
                        Team KPRIEnT & KPRCAnS'''.format(user.username, request.POST['password'])
            send_email(new_user.college_email, subject, content)
            return redirect('user_admin')

        elif 'delete_username' in request.POST:
            login = Login.objects.filter(username=request.POST['delete_username']).first()
            if not login:
                messages.error(request, 'Username "{}" is Not found'.format(request.POST['delete_username']))
                return render(request, 'user_admin_dashboard.html', {'delete_username':request.POST['delete_username']})

            if data['role'] == 'user_admin' and (login.role == 'super_admin' or login.role == 'developer' or login.role == 'user_admin'):
                messages.error(request, 'Dear User Admin, You dont have the privilege to delete this "{}" user'.format(request.POST['delete_username']))
                return render(request, 'user_admin_dashboard.html', {'delete_username':request.POST['delete_username']})

            user = User.objects.filter(login_id=login.id).first()
            if user:
                user.delete()

            login.delete()
            [s.delete() for s in Session.objects.all() if s.get_decoded().get('_auth_user_id') == str(login.id)]
            messages.success(request, 'Username "{}" has been REMOVED successfully'.format(login.username))
            subject = 'Account removed - KPR USER SERVICES'
            content = '''Dear {},\n
                        Your account has been REMOVED by the User Admin\n\n
                        Thanks & Regards,
                        Team KPRIEnT & KPRCAnS'''.format(user.first_name)
            send_email(user.college_email, subject, content)
            return redirect('user_admin')

        elif 'block_user' in request.POST:
            user = Login.objects.filter(username=request.POST['block_user']).first()
            if not user:
                messages.error(request, 'Username "{}" is Not found'.format(request.POST['block_user']))
                return render(request, 'user_admin_dashboard.html', {'block_user':request.POST['block_user']})

            if data['role'] == 'user_admin' and (user.role == 'user_admin' or user.role == 'developer' or user.role == 'super_admin'):
                messages.error(request, 'Dear User Admin, You dont have the privilege to BLOCK this "{}" SUPER USER'.format(request.POST['block_user']))
                return render(request, 'user_admin_dashboard.html', {'block_user':request.POST['block_user']})

            user.role = 'blocked_user'
            [s.delete() for s in Session.objects.all() if s.get_decoded().get('_auth_user_id') == str(user.id)]
            user.save()
            messages.success(request, 'Username "{}" has been BLOCKED successfully'.format(request.POST['block_user']))
            subject = 'Account blocked - KPR USER SERVICES'
            content = '''Dear {},\n
                        Your account has been BLOCKED by the User Admin\n\n
                        Thanks & Regards,\n
                        Team KPRIEnT & KPRCAnS'''.format(user.username)
            send_email(user.username, subject, content)
            return redirect('user_admin')

        elif 'change_role_username' in request.POST:
            user = Login.objects.filter(username=request.POST['change_role_username']).first()
            if not user:
                messages.error(request, 'Username "{}" is Not found'.format(request.POST['change_role_username']))
                return render(request, 'user_admin_dashboard.html', {
                    'change_role_username': request.POST['change_role_username'],
                    'change_role':request.POST['change_role']
                    })
            if data['role'] == 'user_admin' and (user.role == 'user_admin' or user.role == 'developer' or user.role == 'super_admin'):
                messages.error(request, 'Dear User Admin, You dont have the privilege to CHANGE this "{}" USER ROLE'.format(request.POST['change_role_username']))
                return render(request, 'user_admin_dashboard.html', {
                    'change_role_username': request.POST['change_role_username'],
                    'change_role':request.POST['change_role']
                    })

            user.role = request.POST['change_role']
            user.save()
            [s.delete() for s in Session.objects.all() if s.get_decoded().get('_auth_user_id') == str(user.id)]
            messages.success(request, 'Username "{}" ROLE has been changed successfully'.format(request.POST['change_role']))
            subject = 'Role Changed - KPR USER SERVICES'
            content = '''Dear {},\n
                        Your ROLE has been CHANGED by the User Admin\n
                        Your new role "{}"\n
                        Thanks & Regards,
                        Team KPRIEnT & KPRCAnS'''.format(user.username, request.POST['change_role'])
            send_email(user.username, subject, content)
            return redirect('user_admin')

        elif 'reset_username' in request.POST:
            user = Login.objects.filter(username=request.POST['reset_username']).first()
            if not user:
                messages.error(request, '"{}" User Not available'.format(request.POST['reset_username']))
                return render(request, 'user_admin_dashboard.html', {'reset_username': request.POST['reset_username']})

            if data['role'] == 'user_admin' and (user.role == 'user_admin' or user.role == 'developer' or user.role == 'super_admin'):
                messages.error(request, "Dear User Admin, You dont have the privilege to CHANGE this '{}' USER's Password".format(request.POST['reset_username']))
                return render(request, 'user_admin_dashboard.html', {'reset_username': request.POST['reset_username']})

            user.password = make_hash(request.POST['reset_password'])
            [s.delete() for s in Session.objects.all() if s.get_decoded().get('_auth_user_id') == str(user.id)]
            user.save()
            messages.success(request, 'Username "{}" PASSWORD has been RESETED successfully'.format(request.POST['reset_username']))
            subject = 'Password Reseted - KPR USER SERVICES'
            content = '''Dear {},\n
                        Your PASSWORD has been RESETED by the User Admin\n
                        Your new Password: {}\n
                        Thanks & Regards,
                        Team KPRIEnT & KPRCAnS'''.format(user.username, request.POST['reset_password'])
            send_email(user.username, subject, content)
            return redirect('user_admin')
        elif 'delete_all' in request.POST:
            if not request.POST['delete_all'].isdigit() or len(request.POST['delete_all']) != 4:
                messages.error(request, 'Give Valid input [ Ex : 2016 ]')
                return render(request, 'user_admin_dashboard.html', {'delete_all':request.POST['delete_all']})

            batch = request.POST['delete_all'][-2:]
            Login.objects.filter(username__startswith=batch).delete()
            User.objects.filter(college_email__startswith=batch).delete()
            messages.success(request, 'Success, All the ** {} **  batch Students are successfully removed from the Database'.format(request.POST['delete_all']))
            return redirect('user_admin')

    return render(request, 'user_admin_dashboard.html')

def list_user(request):
    if 'kpr_ticket' not in request.COOKIES or not request.user.is_authenticated:
        messages.warning(request, 'Please Login')
        return redirect('login')

    user = check_user(request.COOKIES['kpr_ticket'])
    if not user:
        messages.warning(request, 'You are not admin for KPR USER SERVICE')
        return redirect('services')

    sort = 'id'
    ascending = 0
    descending = 0
    search_keyword = ''
    if 'sort' in request.GET and 'ascending' in request.GET and request.GET['ascending'] == '1':
        ascending = 1
        sort = request.GET['sort']
        users_list = User.objects.order_by(sort)
    elif 'sort' in request.GET and 'descending' in request.GET and request.GET['descending'] == '1':
        descending = 1
        sort = request.GET['sort']
        users_list = User.objects.order_by('-' + sort)
    elif 'search_keyword' in request.GET:
        search_keyword = request.GET['search_keyword'].title()
        users_list = User.objects.filter(first_name__contains=search_keyword)
    else:
        users_list = User.objects.order_by('-created_at')

    login_list = Login.objects.filter()
    user_details = []
    for user in users_list:
        for login in login_list:
            if user.login_id == login.id:
                detail = {
                    'id': user.id,
                    'login_id': user.login_id,
                    'college_email': user.college_email,
                    'role': login.role,
                    'first_name' :user.first_name,
                    'last_name': user.last_name,
                    'gender': user.gender,
                    'phone': user.phone,
                    'profile_photo':user.profile_photo,
                    'department' :user.department,
                    'designation': user.designation,
                    'year' :user.year,
                    'section' :user.section,
                    'created_at' :user.created_at,
                    'updated_at' :user.updated_at,
                }
                user_details.append(detail)
    paginator = Paginator(user_details, 10)
    page = request.GET.get('page')
    try:
        users = paginator.page(page)
    except PageNotAnInteger:
        users = paginator.page(1)
    except EmptyPage:
        users = paginator.page(paginator.num_pages)

    return render(request, 'list_user.html', {
        "users": users,
        'sort': sort,
        'search_keyword': search_keyword,
        'descending': descending,
        'ascending': ascending,
    })

