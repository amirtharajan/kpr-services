from django.urls import include, path
from django.conf import settings
from django.conf.urls.static import static

from django.conf.urls import handler404, handler500
from user_service.views import errors

urlpatterns = [
    path('', include('user_service.urls.public_urls')),
    path('services/admin/', include('user_service.urls.admin_urls')),
    path('services/appointment/', include('appointment_service.urls')),

    path('services/playground/', include('playground_service.urls.common_urls')),
    path('services/playground/admin/', include('playground_service.urls.admin_urls')),

    path('services/food-court/', include('food_service.urls.common_urls')),
    path('services/food-court/admin/', include('food_service.urls.admin_urls')),
]

handler404 = errors.error_404
handler500 = errors.error_500

urlpatterns = urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)