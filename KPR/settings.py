import os

from dotenv import load_dotenv
from pathlib import Path


env_path = Path('.') / '.env'
load_dotenv(dotenv_path=env_path)

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

SECRET_KEY = os.getenv('AUTH_SECRET_KEY')

JWT_SECRET_KEY = os.getenv('JWT_SECRET')
JWT_ALGORITHM = os.getenv('JWT_ALGORITHM')

SALT = os.getenv('SALT')
HASHER = os.getenv('HASHER')

DEBUG = True
# DEBUG = False

ALLOWED_HOSTS = ['localhost','fb14a9bf.ngrok.io']

INSTALLED_APPS = [
    'django_extensions',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # 'django_seed',
    'appointment_service',
    'user_service',
    'playground_service',
    'food_service',
]

AUTH_USER_MODEL = 'user_service.Login'

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    # 'whitenoise.middleware.WhiteNoiseMiddleware',
]

# STATICFILES_STORAGE = 'whitenoise.storage.CompressedStaticFilesStorage'
# WHITENOISE_USE_FINDERS = True

ROOT_URLCONF = os.getenv('ROOT_URLCONF')

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.media',
            ],
        },
    },
]

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
MAILER_EMAIL_BACKEND = EMAIL_BACKEND
EMAIL_HOST = os.getenv('EMAIL_HOST')
EMAIL_HOST_USER = os.getenv('EMAIL_USERNAME')
EMAIL_HOST_PASSWORD = os.getenv('EMAIL_PASSWORD')
EMAIL_PORT = 587
EMAIL_USE_TLS = True

WSGI_APPLICATION = 'KPR.wsgi.application'

DATABASE_ROUTERS = [
    'user_service.routers.UserServiceDatabaceRouter',
    'appointment_service.routers.AppointmentServiceDatabaseRouter',
    'playground_service.routers.PlaygroundServiceDatabaseRouter',
    'food_service.routers.FoodDatabaseRouter',
    ]
DATABASE_APPS_MAPPING = {
    'user_service': 'user_service',
    'appointment_service': 'appointment_service',
    'playground_service': 'playground_service',
    'food_service': 'food_service',
}

DATABASES = {

    'default': {
        'ENGINE': os.getenv('DB_ENGINE'),
        'NAME' : os.getenv('DB_NAME'),
        'HOST': os.getenv('DB_HOST'),
        'PORT' : os.getenv('DB_PORT'),
        'USER' : os.getenv('DB_USERNAME'),
        'PASSWORD' : os.getenv('DB_PASSWORD'),
    },

    'user_service': {
        'ENGINE': os.getenv('DB_ENGINE_USER_SERVICE'),
        'NAME' : os.getenv('DB_NAME_USER_SERVICE'),
        'HOST': os.getenv('DB_HOST_USER_SERVICE'),
        'PORT' : os.getenv('DB_PORT_USER_SERVICE'),
        'USER' : os.getenv('DB_USERNAME_USER_SERVICE'),
        'PASSWORD' : os.getenv('DB_PASSWORD_USER_SERVICE'),
    },

    'appointment_service': {
        'ENGINE': os.getenv('DB_ENGINE_APPOINTMENT'),
        'NAME' : os.getenv('DB_NAME_APPOINTMENT'),
        'HOST': os.getenv('DB_HOST_APPOINTMENT'),
        'PORT' : os.getenv('DB_PORT_APPOINTMENT'),
        'USER' : os.getenv('DB_USERNAME_APPOINTMENT'),
        'PASSWORD' : os.getenv('DB_PASSWORD_APPOINTMENT'),
    },

    'playground_service': {
        'ENGINE': os.getenv('DB_ENGINE_PLAYGROUND'),
        'NAME' : os.getenv('DB_NAME_PLAYGROUND'),
        'HOST': os.getenv('DB_HOST_PLAYGROUND'),
        'PORT' : os.getenv('DB_PORT_PLAYGROUND'),
        'USER' : os.getenv('DB_USERNAME_PLAYGROUND'),
        'PASSWORD' : os.getenv('DB_PASSWORD_PLAYGROUND'),
    },

        'food_service': {
        'ENGINE': os.getenv('DB_ENGINE_FOOD'),
        'NAME' : os.getenv('DB_NAME_FOOD'),
        'HOST': os.getenv('DB_HOST_FOOD'),
        'PORT' : os.getenv('DB_PORT_FOOD'),
        'USER' : os.getenv('DB_USERNAME_FOOD'),
        'PASSWORD' : os.getenv('DB_PASSWORD_FOOD'),
    },
}


AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    }
]

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'Asia/Kolkata'

USE_I18N = True

USE_L10N = True

USE_TZ = True

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static/')

MEDIA_ROOT = os.path.join(BASE_DIR, 'images/')
MEDIA_URL = '/images/'

LOGOUT_REDIRECT = "login.html"

