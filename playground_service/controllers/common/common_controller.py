from django.contrib import messages
from datetime import datetime

from django.shortcuts import render, redirect

from user_service.helpers import verify_kpr_ticket
from user_service.email.email import send_email

from user_service.controllers.public.public_controller import logout

from pluck import pluck
from playground_service.model.PlaygroundModel import Playground
from playground_service.model.PlaygroundBookingModel import PlaygroundBooking

from playground_service.helpers import check_overlap

today = str(datetime.now())
today = today.split(' ')

def playground_booking(request):
    if 'kpr_ticket' not in request.COOKIES:
        messages.warning(request, 'Please Login')
        return redirect('login')

    data = verify_kpr_ticket(request.COOKIES['kpr_ticket'])
    if not data:
        return logout(request)

    playgrounds = Playground.objects.filter(is_visible_on_site=True)
    if request.method == "POST":
        form = {
            'date':request.POST['date_of_required'],
            'start_time':request.POST['start_time'],
            'end_time':request.POST['end_time'],
        }
        ground_id = request.POST['ground_id']
        date_of_required = request.POST['date_of_required']
        start_time = datetime.strptime(request.POST['start_time'], "%H:%M").time()
        end_time = datetime.strptime(request.POST['end_time'], "%H:%M").time()
        
        check_date = str(date_of_required).split()
        if today[0] > check_date[0]:
            messages.error(request, 'Requested date cannot be PAST')
            return render(request,'playground.html',{'playgrounds':playgrounds, 'form':form})

        upper_limit = datetime.strptime('20:01', "%H:%M").time()
        lower_limit = datetime.strptime('06:59', "%H:%M").time()

        if start_time < lower_limit or start_time > upper_limit:
            messages.error(request, 'Playground available only between 7 AM to 8 PM')
            return render(request, 'playground.html', {'playgrounds': playgrounds, 'form':form})

        if end_time <= start_time:
            messages.error(request, 'Ending hour must be after the starting hour')
            return render(request, 'playground.html', {'playgrounds': playgrounds, 'form':form})

        events = PlaygroundBooking.objects.filter(date_of_required=date_of_required, ground_id=ground_id)
        if events:
            for event in events:
                check = check_overlap(event.start_time, event.end_time, start_time, end_time)
                if check:
                    messages.error(request, 'Requested slot is not avilable. Check availability and Retry')
                    return render(request, 'playground.html', {'playgrounds': playgrounds, 'form': form})

        new_booking = PlaygroundBooking(
            ground_id = ground_id,
            login_id = data['id'],
            date_of_booking = datetime.now(),
            date_of_required = date_of_required,
            start_time = start_time,
            end_time = end_time,
        )
        new_booking.save()
        ground_name = Playground.objects.filter(id=ground_id).first()
        messages.success(request, 'Your Playground Booking is Confirmed')
        subject = 'Playground Booking is Confirmed - KPRIET'
        content = '''Dear {}, \n
                    Your Playground Booking is Confirmed\n
                    Ground Name: {},
                    Date: {},
                    Start Time : {},
                    End Time : {}.\n\n
                    Thanks & Regards,
                    KPR Institutions'''.format(data['first_name'], ground_name.name, date_of_required, start_time, end_time)
        send_email(data['username'], subject, content)
        return redirect('playground_booking')

    return render(request,'playground.html',{'playgrounds':playgrounds})

def check_availability(request):
    if 'kpr_ticket' not in request.COOKIES:
        messages.warning(request, 'Please Login')
        return redirect('login')

    data = verify_kpr_ticket(request.COOKIES['kpr_ticket'])
    if not data:
        return logout(request)

    playgrounds = Playground.objects.filter()
    if request.method == 'POST':
        requested_date = request.POST['date']
        requested_date = str(requested_date).split(' ')
        if today[0] > requested_date[0]:
            messages.error(request, 'Requested date cannot be PAST')
            return render(request, 'playground_availability.html', {'playgrounds': playgrounds})

        booking_details = PlaygroundBooking.objects.filter(ground_id=request.POST['ground_id'], date_of_required=requested_date[0])
        ground_name = Playground.objects.filter(id=request.POST['ground_id']).first()
        if not booking_details:
            return render(request, 'playground_availability.html', {'full_day': True, 'playgrounds':playgrounds,  'date':requested_date[0]})

        availability = []
        for no, data in enumerate (booking_details):
            detail = {
                'no' : no + 1,
                'start_time': data.start_time,
                'end_time' : data.end_time,
            }
            availability.append(detail)
        return render(request, 'playground_availability.html', {'availability': availability, 'ground_name':ground_name.name, 'total':len(availability), 'date':requested_date[0], 'playgrounds':playgrounds})

    return render(request, 'playground_availability.html', {'playgrounds':playgrounds})

def my_slots(request):
    if 'kpr_ticket' not in request.COOKIES:
        messages.warning(request, 'Please Login')
        return redirect('login')

    data = verify_kpr_ticket(request.COOKIES['kpr_ticket'])
    if not data:
        return logout(request)

    slots = PlaygroundBooking.objects.filter(date_of_required__gte=today[0], login_id=data['id']).order_by('-date_of_required')
    ground_ids = pluck(slots, 'ground_id')
    grounds = Playground.objects.filter(id__in=ground_ids)
    slot_details = []
    for no, slot in enumerate (slots):
        for data in grounds:
            if slot.ground_id == data.id:
                detail = {
                    'no': no + 1,
                    'booking_id' : slot.id,
                    'name' : data.name,
                    'date_of_required' : slot.date_of_required,
                    'status' : slot.status,
                }
                slot_details.append(detail)

    return render(request, 'my_slots.html', {'slots': slot_details})

def cancel_slot(request):
    PlaygroundBooking.objects.filter(id=request.GET['cancel_booking']).delete()
    messages.success(request, 'Slot canceled')
    return redirect('my_slots')
