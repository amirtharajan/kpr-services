
from django.contrib import messages
from django.shortcuts import render, redirect
import datetime

from user_service.email.email import send_email
from playground_service.helpers import check_user
from pluck import pluck

from django.db.models import Q

from playground_service.model.PlaygroundModel import Playground
from playground_service.model.PlaygroundBookingModel import PlaygroundBooking

from user_service.model.UserModel import User
from user_service.models import Login

today = datetime.datetime.now()


def playground_admin(request):
    if 'kpr_ticket' not in request.COOKIES:
        messages.warning(request, 'Please Login')
        return redirect('login')

    user = check_user(request.COOKIES['kpr_ticket'])
    if not user:
        messages.warning(request, 'You dont have any admin privilege')
        return redirect('appointment')

    if request.method == 'POST':
        if 'cancel_id' in request.POST:
            ground = PlaygroundBooking.objects.filter(id=request.POST['cancel_id']).first()
            user = User.objects.get(login_id=ground.login_id)
            PlaygroundBooking.objects.filter(id=request.POST['cancel_id']).first().delete()
            subject = 'KPR - Playground SLOT Canceled'
            content = '''Dear {} {}, \n
                    Your playground slot has been CANCELED by ADMIN.\n\n
                    Thanks & Regards,
                    KPR Institutions'''.format(user.first_name, user.last_name)
            send_email(user.college_email, subject, content)
            messages.success(request, 'SLOT Canceled')
            return redirect('playground_admin')

    requested_date = None
    if 'requested_date' in request.GET:
        requested_date = request.GET['requested_date']
        requested_date1 = str(requested_date).split(' ')
        bookings = PlaygroundBooking.objects.filter(date_of_required=requested_date1[0])

    elif 'all' in request.GET:
        bookings = PlaygroundBooking.objects.filter(date_of_required__gte=today).order_by('date_of_required')

    else:
        bookings = PlaygroundBooking.objects.filter(date_of_required__gte=today).order_by('date_of_required')

    ground_ids = pluck(bookings, 'ground_id')
    ground_details = Playground.objects.filter(id__in=ground_ids)
    user_details = User.objects.filter(login_id=user['id']).first()
    booking_details = []
    for no, booking in enumerate (bookings):
        for data in ground_details:
            if booking.ground_id == data.id:
                detail = {
                    'no'                :   no+1,
                    'playground_id'     :   data.id,
                    'playground_name'   :   data.name,
                    'playground_type'   :   data.ground_type,
                    'audience_capacity' :   data.audience_capacity,
                    'booking_id'        :   booking.id,
                    'date_of_booking'   :   booking.date_of_booking,
                    'date_of_required'  :   booking.date_of_required,
                    'start_time'        :   booking.start_time,
                    'end_time'          :   booking.end_time,
                    'status'            :   booking.status,
                    'login_id'          :   user_details.login_id,
                    'first_name'        :   user_details.first_name,
                    'last_name'         :   user_details.last_name,
                    'gender'            :   user_details.gender,
                    'college_email'     :   user_details.college_email,
                    'phone'             :   user_details.phone,
                    'department'        :   user_details.department,
                    'designation'       :   user_details.designation,
                    'year'              :   user_details.year,
                    'section'           :   user_details.section,
                }
                booking_details.append(detail)
    return render(request, 'playground_admin.html', {'total': len(booking_details), 'requested_date': requested_date, 'booking_details':booking_details})

def playground_list(request):
    if 'kpr_ticket' not in request.COOKIES:
        messages.warning(request, 'Please Login')
        return redirect('login')

    user = check_user(request.COOKIES['kpr_ticket'])
    if not user:
        messages.warning(request, 'You dont have any admin privilege')
        return redirect('appointment')

    if request.method == 'POST':
        if 'new_playground_name' in request.POST:
            new_ground = Playground(
                name = request.POST['new_playground_name'],
                ground_type = request.POST['ground_type'] if 'ground_type' in request.POST else None,
                audience_capacity = request.POST['audience_capacity'] if 'audience_capacity' in request.POST else None,
            )
            new_ground.save()
            messages.success(request, 'New Playground Added Successfully ')
            return redirect('playground_list')

        elif 'visibility_ground_id' in request.POST:
            ground = Playground.objects.get(id=request.POST['visibility_ground_id'])
            if ground.is_visible_on_site == True:
                ground.is_visible_on_site = False
            else:
                ground.is_visible_on_site = True

            if ground.is_visible_on_site == True:
                m = 'Now, Playground is visible on Site'
            else:
                m = 'Now, Playground is not visible on Site'
            ground.save()
            messages.success(request, m)
            return redirect('playground_list')

        elif 'update_ground_id' in request.POST:
            ground = Playground.objects.get(id=request.POST['update_ground_id'])
            ground.name = request.POST['name'] if 'name' in request.POST else ground.name
            ground.ground_type = request.POST['ground_type'] if 'ground_type' in request.POST else ground.ground_type
            ground.audience_capacity = request.POST['audience_capacity'] if 'audience_capacity' in request.POST else ground.audience_capacity
            ground.save()
            messages.success(request, 'Playground Updated Successfully ')
            return redirect('playground_list')

        elif 'delete_plaground_id' in request.POST:
            Playground.objects.filter(id=request.POST['delete_plaground_id']).delete()
            messages.success(request, 'Playground Deleted Successfully ')
            return redirect('playground_list')

    grounds = Playground.objects.filter()
    ground_details = []
    for data in grounds:
        detail = {
            'id' : data.id,
            'is_visible_on_site' : data.is_visible_on_site,
            'name' : data.name,
            'ground_type' : data.ground_type,
            'audience_capacity' : data.audience_capacity,
        }
        ground_details.append(detail)
    return render(request, 'playground_list.html', {'playground_list': ground_details})
