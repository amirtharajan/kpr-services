from user_service.helpers import verify_kpr_ticket


def check_overlap(fixed_start, fixed_end, new_start, new_end):
    overlap = False
    if new_start == fixed_end or new_end == fixed_start:    #edge case
        overlap = False
    elif (new_start >= fixed_start and new_start <= fixed_end) or (new_end >= fixed_start and new_end <= fixed_end): #innner limits
        overlap = True
    elif new_start <= fixed_start and new_end >= fixed_end: #outter limits
        overlap = True
    return overlap

def check_user(kpr_ticket):
    user = verify_kpr_ticket(kpr_ticket)
    if not user:
        return False

    if user['role'] == 'playground_admin' or user['role'] == 'super_admin':
        return user
    return False
