from django.db import models
from datetime import datetime
from django.core.exceptions import ValidationError

from playground_service.model.PlaygroundModel import Playground

class PlaygroundBooking(models.Model):

    login_id            =   models.IntegerField()
    ground              =   models.ForeignKey(Playground, on_delete=models.CASCADE)
    date_of_booking     =   models.DateField(blank=True, null=True)
    date_of_required    =   models.DateField(blank=True, null=True)
    start_time          =   models.TimeField(blank=True, null=True)
    end_time            =   models.TimeField(blank=True, null=True)
    status              =   models.CharField(default='confirmed', max_length=30, blank=False, null=False)

    objects = models.Manager()

    class Meta:
        db_table = "playgroud_bookings"