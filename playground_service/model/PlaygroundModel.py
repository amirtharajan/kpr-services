from django.db import models

class Playground(models.Model):

    is_visible_on_site  =   models.BooleanField(default=True)
    name                =   models.CharField(max_length=255, blank=True, null=True)
    ground_type         =   models.CharField(max_length=255, blank=True, null=True)
    audience_capacity   =   models.IntegerField(default=0, blank=True, null=True)
    created_at          =   models.DateTimeField(auto_now_add=True, blank=True, null=True)
    updated_at          =   models.DateTimeField(auto_now=True, blank=True, null=True)

    objects = models.Manager()

    class Meta:
        db_table = "playgrounds"
