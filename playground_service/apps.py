from django.apps import AppConfig


class PlaygroundServiceConfig(AppConfig):
    name = 'playground_service'
