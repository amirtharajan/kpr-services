
from django.urls import path

from playground_service.controllers.common.common_controller import (
    playground_booking,
    check_availability,
    my_slots,
    cancel_slot
)


urlpatterns = [
    path('', playground_booking, name='playground_booking'),
    path('availability/', check_availability, name='check_availability'),
    path('my-slots/', my_slots, name='my_slots'),
    path('cancel/', cancel_slot, name='cancel_slot'),
]
