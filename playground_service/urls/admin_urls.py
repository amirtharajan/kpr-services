from django.urls import path

from playground_service.controllers.admin.admin_controller import (
    playground_admin,
    playground_list,
)


urlpatterns = [
    path('', playground_admin, name='playground_admin'),
    path('playgrounds/', playground_list, name='playground_list'),
]
