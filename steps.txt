If you want run this application from your machine, please execute the following 7 steps.

	Step 1 : Download & Install MySQL Server

	Step 2 : Download & Install Python3

	Step 3 : Clone the project from the GitLab

	Step 4 : Ensure “.env” file in your cloned project folder

	Step 5 : Ensure “requirements.txt” file in your cloned project folder

	Step 6 : Create new database in your MySQL named “kpr.user_service”
			 Create new database in your MySQL named “kpr.appointment_service”
			 Create new database in your MySQL named “kpr.playground_service”
			 Create new database in your MySQL named “kpr.food_service”
		(Recommend:  Download HEIDISQL to manage your MySQL server in User Friendly from the following link)
		https://www.heidisql.com/download.php

	Step 7 : Run the following command from your cloned project folder
		pip install -r requirements.txt
		python manage.py makemigrations
		python manage.py migrate --database 'user_service'
		python manage.py migrate --database 'playground_service'
		python manage.py migrate --database 'appointment_service'
		python manage.py migrate --database 'food_service'
		python manage.py runserver

------------------------------------------- *** The END *** -------------------------------------------------