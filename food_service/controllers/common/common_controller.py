from django.http import HttpResponse, HttpResponseRedirect
from datetime import datetime
from django.contrib import messages
from django.shortcuts import redirect, render, reverse
from food_service.helpers import verify_kpr_ticket, create_wallet
from food_service.email import send_email

from food_service.models.FoodItemModel import FoodItem
from food_service.models.OrderModel import Order
from food_service.models.CartModel import Cart
from food_service.models.WalletModel import Wallet
from food_service.models.SpecialItemsModel import SpecialItem
from food_service.models.MainImageModel import MainImage

from django.urls import reverse

from pluck import pluck
import json
import re


today_datetime = str(datetime.now()).split(' ')
today = today_datetime[0]


def list_items(request):
    if 'kpr_ticket' not in request.COOKIES:
        return redirect('login')
    # else:
    #     check = requests.get(VERIFY_USER, params={'id':request.COOKIES['kpr_ticket']})
    #     if not check:
    #         return HttpResponseRedirect(LOGIN)

    data = verify_kpr_ticket(request.COOKIES['kpr_ticket'])
    if not data:
        return redirect('login')

    check_wallet = Wallet.objects.filter(login_id=data['id']).first()
    if not check_wallet:
        create_wallet(data['id'], data['username'])

    if request.method == 'GET':
        if 'order_item_id' in request.GET:
            check_item_id = Cart.objects.filter(
                login_id=data['id'], item_id=request.GET['order_item_id']).first()
            if check_item_id:
                check_item_id.count += int(request.GET['item_count'])
                check_item_id.save()
                messages.success(request, 'Added into cart')
                return redirect("food")
            else:
                new_cart = Cart(
                    login_id=data['id'],
                    item_id=request.GET['order_item_id'],
                    count=request.GET['item_count'],
                )
                new_cart.save()
                messages.success(request, 'Added into cart')
                return redirect("food")
    
    if 'veg_only' in request.GET:
        items = FoodItem.objects.filter(
            is_available=True, available_count__gte=1, food_family='veg')
    elif 'all' in request.GET:
        items = FoodItem.objects.filter(
            is_available=True, available_count__gte=1)
    elif 'search_category' in request.GET and len(request.GET['search_category']) != 0:
        items = FoodItem.objects.filter(
            is_available=True, available_count__gte=1, type=request.GET['search_category'])
    elif 'search_category' in request.GET and len(request.GET['search_category']) == 0:
        items = FoodItem.objects.filter(
            is_available=True, available_count__gte=1)
    else:
        items = FoodItem.objects.filter(
            is_available=True, available_count__gte=1)

    carts = Cart.objects.filter(login_id=data['id'])
    cart_total = 0
    if not carts:
        cart_total = 0
    else:
        for d in carts:
            cart_total += d.count
    tiffen = []
    meals = []
    cool_beverages = []
    hot_beverages = []
    chips_biscuits = []
    snacks = []
    fried_rice = []
    noodles = []
    biriyani = []
    parota_chappathi = []
    egg = []
    others = []
    now = str(datetime.now()).split()
    current_time = datetime.strptime(now[1], "%H:%M:%S.%f").time()
    for item in items:
        if current_time >= item.available_from and current_time <= item.available_to:
            if item.type == 'tiffen':
                detail = {
                    'id': item.id,
                    'name': item.name,
                    'photo': item.photo if item.photo else 'images/no_image1.jpg',
                    'available_count': item.available_count,
                    'prize': item.prize,
                    'food_family': item.food_family,
                    'from': item.available_from,
                    'to': item.available_to,
                }
                tiffen.append(detail)
            elif item.type == 'meals':
                detail = {
                    'id': item.id,
                    'name': item.name,
                    'photo': item.photo if item.photo else 'images/no_image1.jpg',
                    'available_count': item.available_count,
                    'prize': item.prize,
                    'food_family': item.food_family,
                    'from': item.available_from,
                    'to': item.available_to,
                }
                meals.append(detail)
            elif item.type == 'cool_beverages':
                detail = {
                    'id': item.id,
                    'name': item.name,
                    'photo': item.photo if item.photo else 'images/no_image1.jpg',
                    'available_count': item.available_count,
                    'prize': item.prize,
                    'food_family': item.food_family,
                    'from': item.available_from,
                    'to': item.available_to,
                }
                cool_beverages.append(detail)
            elif item.type == 'hot_beverages':
                detail = {
                    'id': item.id,
                    'name': item.name,
                    'photo': item.photo if item.photo else 'images/no_image1.jpg',
                    'available_count': item.available_count,
                    'prize': item.prize,
                    'food_family': item.food_family,
                    'from': item.available_from,
                    'to': item.available_to,
                }
                hot_beverages.append(detail)
            elif item.type == 'chips_biscuits':
                detail = {
                    'id': item.id,
                    'name': item.name,
                    'photo': item.photo if item.photo else 'images/no_image1.jpg',
                    'available_count': item.available_count,
                    'prize': item.prize,
                    'food_family': item.food_family,
                    'from': item.available_from,
                    'to': item.available_to,
                }
                chips_biscuits.append(detail)
            elif item.type == 'snacks':
                detail = {
                    'id': item.id,
                    'name': item.name,
                    'photo': item.photo if item.photo else 'images/no_image1.jpg',
                    'available_count': item.available_count,
                    'prize': item.prize,
                    'food_family': item.food_family,
                    'from': item.available_from,
                    'to': item.available_to,
                }
                snacks.append(detail)
            elif item.type == 'fried_rice':
                detail = {
                    'id': item.id,
                    'name': item.name,
                    'photo': item.photo if item.photo else 'images/no_image1.jpg',
                    'available_count': item.available_count,
                    'prize': item.prize,
                    'food_family': item.food_family,
                    'from': item.available_from,
                    'to': item.available_to,
                }
                fried_rice.append(detail)
            elif item.type == 'noodles':
                detail = {
                    'id': item.id,
                    'name': item.name,
                    'photo': item.photo if item.photo else 'images/no_image1.jpg',
                    'available_count': item.available_count,
                    'prize': item.prize,
                    'food_family': item.food_family,
                    'from': item.available_from,
                    'to': item.available_to,
                }
                noodles.append(detail)
            elif item.type == 'biriyani':
                detail = {
                    'id': item.id,
                    'name': item.name,
                    'photo': item.photo if item.photo else 'images/no_image1.jpg',
                    'available_count': item.available_count,
                    'prize': item.prize,
                    'food_family': item.food_family,
                    'from': item.available_from,
                    'to': item.available_to,
                }
                biriyani.append(detail)
            elif item.type == 'parota_chappathi':
                detail = {
                    'id': item.id,
                    'name': item.name,
                    'photo': item.photo if item.photo else 'images/no_image1.jpg',
                    'available_count': item.available_count,
                    'prize': item.prize,
                    'food_family': item.food_family,
                    'from': item.available_from,
                    'to': item.available_to,
                }
                parota_chappathi.append(detail)
            elif item.type == 'egg':
                detail = {
                    'id': item.id,
                    'name': item.name,
                    'photo': item.photo if item.photo else 'images/no_image1.jpg',
                    'available_count': item.available_count,
                    'prize': item.prize,
                    'food_family': item.food_family,
                    'from': item.available_from,
                    'to': item.available_to,
                }
                egg.append(detail)
            elif item.type == 'others':
                detail = {
                    'id': item.id,
                    'name': item.name,
                    'photo': item.photo if item.photo else 'images/no_image1.jpg',
                    'available_count': item.available_count,
                    'prize': item.prize,
                    'food_family': item.food_family,
                    'from': item.available_from,
                    'to': item.available_to,
                }
                others.append(detail)
    balance = Wallet.objects.filter(login_id=data['id']).first()
    spl_items = SpecialItem.objects.filter(date=today)
    spl_item_ids = pluck(spl_items, 'item_id')
    spl_item_details = FoodItem.objects.filter(id__in=spl_item_ids)
    today_special = []

    main_photo = MainImage.objects.filter(id=1).first()
    if not main_photo:
        main_photo = MainImage(main_photo='images/none.jpg')
        main_photo.save()

    for no, d in enumerate(spl_items):
        for item in spl_item_details:
            if d.item_id == item.id:
                detail = {
                    'no': no+1,
                    'name': item.name,
                    'photo': d.photo,
                    'title': d.title,
                    'description': d.description,
                }
                today_special.append(detail)
    return render(request, 'home.html', {
        'today_special': today_special,
        'balance': balance.balance,
        'cart_total': cart_total,
        'user': data,
        'tiffen': tiffen,
        'meals': meals,
        'cool_beverages': cool_beverages,
        'hot_beverages': hot_beverages,
        'chips_biscuits': chips_biscuits,
        'snacks': snacks,
        'fried_rice': fried_rice,
        'noodles': noodles,
        'biriyani': biriyani,
        'parota_chappathi': parota_chappathi,
        'egg': egg,
        'others': others,
        'none': None,
        'main_photo': main_photo.main_photo
    })

def make_order(request):
    if 'kpr_ticket' not in request.COOKIES:
        return redirect('login')

    data = verify_kpr_ticket(request.COOKIES['kpr_ticket'])
    if not data:
        return redirect('login')

    cart_items = Cart.objects.filter(login_id=data['id'])

    if not cart_items:
        messages.info(request, 'Your cart is empty')
        return redirect('food')

    if 'cancel_id' in request.GET:
        Cart.objects.filter(id=request.GET['cancel_id']).delete()
        return redirect('cart')

    cart_ids = pluck(cart_items, 'id')
    item_ids = pluck(cart_items, 'item_id')
    item_details = FoodItem.objects.filter(id__in=item_ids)
    cart_items_details = []
    order_details = []
    total_cost = 0
    for d in cart_items:
        for item in item_details:
            if d.item_id == item.id:
                total_cost += d.count * item.prize
                detail = {
                    'id': d.id,
                    'name': item.name,
                    'quantity': d.count,
                    'total': d.count * item.prize
                }
                o_detail = {
                    item.name: d.count
                }
                order_details.append(o_detail)
                cart_items_details.append(detail)

    total_order_cost = 0
    check_wallet = Wallet.objects.get(login_id=data['id'])
    if request.method == 'POST':
        now = str(datetime.now()).split('.')
        now = now[0].split()
        current_time = datetime.strptime(now[1], "%H:%M:%S").time()

        for d, item in zip(cart_items, item_details):
            total_order_cost += d.count * item.prize

            if d.item_id == item.id and item.available_count < d.count or item.available_count == 0:
                get_item_name = FoodItem.objects.filter(id=item.id).first()
                messages.info(request, '{}, Available {} Nos only'.format(get_item_name.name, item.available_count))
                return redirect('cart')

            if current_time <= item.available_from or current_time >= item.available_to or item.is_available == False:
                get_item_name = FoodItem.objects.filter(id=item.id).first()
                messages.info(request, '{}, "NOT AVAILABLE" [ Closed by FC ]. '.format(get_item_name.name,))
                return redirect('cart')

        if check_wallet.balance < total_cost:
            messages.info(request, 'Wallet Balance is not sufficient')
            return redirect('cart')

        new_order = Order(
            login_id=data['id'],
            user_details=json.dumps(data),
            items=json.dumps(order_details),
            total_cost=total_order_cost,
            ordered_date=today_datetime[0],
            ordered_time=today_datetime[1],
        )
        new_order.save()
        Cart.objects.filter(id__in=cart_ids).delete()
        for d in item_details:
            for item in cart_items:
                if d.id == item.item_id:
                    d.available_count -= item.count
                    d.save()

        messages.success(request, 'Success, Your order is placed')
        check_wallet.balance -= total_cost
        check_wallet.save()
        return redirect('food')

    return render(request, 'cart.html', {'balance': check_wallet.balance, 'cart_items_details': cart_items_details, 'net_total': total_cost})

def my_order(request):
    if 'kpr_ticket' not in request.COOKIES:
        return redirect('login')

    data = verify_kpr_ticket(request.COOKIES['kpr_ticket'])
    if not data:
        return redirect('login')

    orders = Order.objects.filter(
        login_id=data['id'], status='confirmed', ordered_date=today)

    order_details = []
    for order in orders:
        items = order.items
        char_list = ['[', '{', "'", '"', '}', ']']
        items = re.sub("|".join(char_list), "", items)
        items = items.replace('[', '')
        items = items.replace(']', '')
        items = items.split(', ')
        detail = {
            'id': order.id,
            'items': items,
            'status': order.status,
            'total_cost': order.total_cost,
        }
        order_details.append(detail)
        if 'order_id' in request.GET:
            Order.objects.filter(id=request.GET['order_id']).update(status='delivered',delivery_time=today_datetime[1])
            messages.add_message(request, messages.SUCCESS, 'Order ID: {},\n\n Items: {}, \n\nDelivered'.format(
                request.GET['order_id'], order_details[0]['items']))
            send_email(
                data['username'],
                'FC Order ID {} Delivered'.format(request.GET['order_id']),
                '''Dear {}, \n
                KPR Food Court - Order Details\n
                Order ID: {},
                Items: {},
                Total Cost : {},
                Date : {},
                Time : {},\n
                Your Order has been Delivered Successfully.\n
                Enjoy your food.
                Thank you, visit Again\n

                Thanks & Regards,
                KPR Food Court
                '''.format(
                    data['first_name'],
                    order_details[0]['id'],
                    order_details[0]['items'],
                    order_details[0]['total_cost'],
                    today_datetime[0],
                    today_datetime[1],
                ))
            return redirect('my_order')

    return render(request, 'order.html', {'order_details': order_details})
