from django.contrib import messages
from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from datetime import datetime

from django.core.paginator import Paginator
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


from food_service.helpers import verify_kpr_ticket, image_handeller, image_handeller_high_quality
from food_service.email import send_email

from food_service.models.FoodItemModel import FoodItem
from food_service.models.OrderModel import Order
from food_service.models.WalletModel import Wallet
from food_service.models.MainImageModel import MainImage
from food_service.models.SpecialItemsModel import SpecialItem

from food_service.models.FoodItemModel import NewItemFrom

from django.db.models import Sum
import re

today_datetime = str(datetime.now()).split(' ')
today = today_datetime[0]


def get_item(id):
    try:
        food_item = FoodItem.objects.filter(id=id).first()
        if food_item:
            return food_item

        return None
    except:
        return None

def filter(field, order):
    if order == 'ascending':
        foods = FoodItem.objects.order_by(field)
        return foods
    else:
        foods = FoodItem.objects.order_by('-'+field)
        return foods

def list_all(request):
    if 'kpr_ticket' not in request.COOKIES:
        messages.warning(request, 'Please Login')
        return redirect('login')

    user = verify_kpr_ticket(request.COOKIES['kpr_ticket'])
    if not user:
        messages.warning(request, 'Please Login')
        return redirect('login')

    if user['role'] != 'super_admin' and user['role'] != 'food_admin':
        messages.error(request, 'You are not admin for KPR FOOD COURT')
        return redirect('food')

    sort = ''
    ascending = 0
    descending = 0
    search_keyword = ''
    if 'sort' in request.GET and 'ascending' in request.GET and request.GET['ascending'] == '1':
        ascending = 1
        sort = request.GET['sort']
        foods = FoodItem.objects.order_by(sort)
    elif 'sort' in request.GET and 'descending' in request.GET and request.GET['descending'] == '1':
        descending = 1
        sort = request.GET['sort']
        foods = FoodItem.objects.order_by('-' + sort)
    elif 'search_keyword' in request.GET:
        search_keyword = request.GET['search_keyword'].title()
        foods = FoodItem.objects.filter(name__contains=search_keyword)
    else:
        foods = FoodItem.objects.order_by('-created_at')
    sales = Order.objects.filter(ordered_date=today).aggregate(total_price=Sum('total_cost'))

    food_details = []
    paginator = Paginator(foods, 12)
    page = request.GET.get('page')
    try:
        foods = paginator.page(page)
    except PageNotAnInteger:
        foods = paginator.page(1)
    except EmptyPage:
        foods = paginator.page(paginator.num_pages)

    for d in foods:
        detail = {
            'id':d.id,
            'name': d.name,
            'photo': d.photo if d.photo else None,
            'is_available': d.is_available,
            'available_count': d.available_count,
            'prize': d.prize,
            'food_family': d.food_family,
            'type': d.type,
            'available_from': d.available_from,
            'available_to': d.available_to,
        }
        food_details.append(detail)

    if request.method == 'POST':
        if 'photo' in request.FILES:
            name = str(request.POST['name']).split('.')
            image_file = image_handeller(request.FILES['photo'], name[0])
        else:
            image_file = 'images/no_image1.jpg'

        if 'add_food_item' in request.POST:
            form = {
                'name' : request.POST['name'].title(),
                'food_family' : request.POST['food_family'],
                'type' : request.POST['type'],
                'prize' : request.POST['prize'],
                'available_count': request.POST['available_count'] if 'available_count' in request.POST else 0,
                'available_from': request.POST['available_from'],
                'available_to' : request.POST['available_to'],
            }
            if not image_file:
                messages.error(request, 'JPG or JPEG format only allowed for FOOD ITEM PHOTO')
                return render(request, 'food_admin.html', {'form': form,})

            new_item = FoodItem(
                name = form['name'],
                photo = image_file,
                food_family = form['food_family'],
                is_available = True,
                available_count = form['available_count'] if form['available_count'].isdigit() else 0,
                prize=form['prize'],
                type=form['type'],
                available_from=form['available_from'],
                available_to=form['available_to'],
                created_at =datetime.now()
            )
            new_item.save()
            messages.success(request, 'New Food Item Added')
            return redirect('food_admin')

        elif 'update_food_item' in request.POST:
            if not image_file:
                messages.error(request, 'JPG or JPEG format only allowed for FOOD ITEM PHOTO')
                return redirect('food_admin')

            update_item = FoodItem.objects.filter(id=request.POST['id']).first()
            update_item.name = request.POST['name'] if 'name' in request.POST else update_item.name
            update_item.photo = image_file if 'photo' in request.FILES else update_item.photo
            update_item.is_available =  request.POST['is_available'] if 'is_available' in request.POST else update_item.is_available
            update_item.available_count =  request.POST['available_count'] if 'available_count' in request.POST else update_item.available_count
            update_item.prize =  request.POST['prize'] if 'prize' in request.POST else update_item.prize
            update_item.food_family =  request.POST['food_family'] if 'food_family' in request.POST else update_item.food_family
            update_item.type =  request.POST['type'] if 'type' in request.POST else update_item.type
            update_item.available_from =  request.POST['available_from'] if 'available_from' in request.POST else update_item.available_from
            update_item.available_to =  request.POST['available_to'] if 'available_to' in request.POST else update_item.available_to
            update_item.updated_at = datetime.now()
            update_item.save()
            messages.success(request, "{} details Successfully updated".format(update_item.name))
            return redirect('food_admin')

        elif 'change_visibility_item_id' in request.POST:
            change_visibility_item = get_item(request.POST['change_visibility_item_id'])
            if not change_visibility_item:
                messages.warning(request, 'Item not found')
                return redirect('food_admin')

            if change_visibility_item.is_available == True:
                change_visibility_item.is_available = False
                messages.warning(request, '{}, Now Closed for order'.format(change_visibility_item.name))
                change_visibility_item.save()
                return redirect('food_admin')

            change_visibility_item.is_available = True
            change_visibility_item.save()
            messages.success(request, '{}, Now Open for order'.format(change_visibility_item.name))
            return redirect('food_admin')

        elif 'delete_food_id' in request.POST:
            delete_food_item = get_item(request.POST['delete_food_id'])
            if not delete_food_item:
                messages.warning(request, 'Item not found')
                return redirect('food_admin')

            delete_food_item.delete()
            messages.success(request, '{}, Successfully removed from the database'.format(delete_food_item.name))
            return redirect('food_admin')

        elif 'change_available_count_item_id' in request.POST:
            FoodItem.objects.filter(id=request.POST['change_available_count_item_id']).update(available_count=request.POST['available_count'])
            messages.success(request, 'Available count updated')
            return redirect('food_admin')

    return render(request, 'food_admin.html',
        {
        'all_foods': food_details,
        'foods':foods,
        'sales': sales,
        'sort': sort,
        'descending': descending,
        'ascending': ascending,
        'search_keyword':search_keyword,
        })

def food_admin(request):
    if 'kpr_ticket' not in request.COOKIES:
        messages.warning(request, 'Please Login')
        return redirect('login')

    user = verify_kpr_ticket(request.COOKIES['kpr_ticket'])
    if not user:
        messages.warning(request, 'Please Login')
        return redirect('login')

    if user['role'] != 'super_admin' and user['role'] != 'food_admin':
        messages.error(request, 'You are not admin for KPR FOOD COURT')
        return redirect('food')

    sales = Order.objects.filter(ordered_date=today).aggregate(total_price=Sum('total_cost'))
    food_item_names = FoodItem.objects.order_by('name').only('id', 'name')

    if 'sales_from_date' in request.GET:
        sale_from_date = request.GET['sales_from_date']
        sale_to_date = request.GET['sales_to_date']
        requested_sales = Order.objects.filter(ordered_date__gte=sale_from_date, ordered_date__lte=sale_to_date).aggregate(total_price=Sum('total_cost'))
        return render(
        request, 'food_admin_1.html',
        {
            'requested_sales': requested_sales,
            'sale_from_date': sale_from_date,
            'sale_to_date': sale_to_date,
            'sales': sales,
            'food_item_names': food_item_names,
        }
    )

    elif 'wallet_email' in request.GET:
        wallet = Wallet.objects.filter(username=request.GET['wallet_email']).first()
        if not wallet:
            messages.error(request, 'Wallet not, Request the User to Login the Food Court Application, After Try to recharge')
            return render(request, 'food_admin_1.html',
                {
                    'today': today,
                    'sales': sales,
                    'wallet_email': request.GET['wallet_email'],
                    'wallet_amount': request.GET['wallet_amount'],
                    'food_item_names': food_item_names,
                }
            )
        if not request.GET['wallet_amount'].isdigit():
            messages.error(request, 'Enter Valid Amount')
            return render(request, 'food_admin_1.html',
                {
                    'today': today,
                    'sales': sales,
                    'wallet_email': request.GET['wallet_email'],
                    'wallet_amount': request.GET['wallet_amount'],
                    'food_item_names': food_item_names,
                }
            )
        wallet.balance += int(request.GET['wallet_amount'])
        wallet.save()
        send_email(
            wallet.username,
            'Wallet Recharged',
            '''Dear {}, \n
            KPR Food Court - Wallet Details\n
            Wallet ID: {},
            Recharge Amount: {},
            Total Amount : {}\n
            Your Wallet has been Recharged Successfully.\n
            Buy & Enjoy your food.
            Thank you, visit Again\n

            Thanks & Regards,
            KPR Food Court
            '''.format(
                wallet.username,
                wallet.id,
                request.GET['wallet_amount'],
                wallet.balance,
            )
        )
        messages.success(request, 'Wallet Recharged')
        return redirect("details")

    elif 'get_order_details' in request.GET:
        order_details = Order.objects.filter(id=request.GET['order_id']).first()
        if not order_details:
            messages.warning(request, "No data found")
            return render(request, 'food_admin_1.html', {'order_id': request.GET['order_id'],})

        items = change_format(order_details.items)
        order_user_details = change_format(order_details.user_details)
        order_detail = {
            'id': order_details.id,
            'items': items,
            'order_user_details':order_user_details[0:len(order_user_details)-2],
            'status': order_details.status,
            'total_cost': order_details.total_cost,
            'order_date': order_details.ordered_date,
            'order_time': order_details.ordered_time,
            'delivery_time':order_details.delivery_time,
        }
        return render(request, 'food_admin_1.html', {'order_id': request.GET['order_id'], 'order_detail': order_detail, 'food_item_names': food_item_names,})

    if request.method == 'POST':
        if 'change_main_photo' in request.FILES:
            IMAGE_FILE_TYPES = ['jpg', 'png', 'PNG', 'jpeg', 'JPG', 'JPEG', 'gif', 'GIF']
            main_image_file = str(request.FILES['change_main_photo']).split('.')
            if main_image_file[-1] not in IMAGE_FILE_TYPES:
                messages.warning(request, 'Only JPG, JPEG, PNG and GIF allowed')
                return redirect('details')

            MainImage.objects.update_or_create(id=1, defaults={'main_photo': request.FILES['change_main_photo']})
            messages.success(request, 'Image uploaded')
            return redirect('details')
        elif 'special_food' in request.POST:
            spl_image_file = image_handeller_high_quality(request.FILES['special_photo'], 'special')
            if not spl_image_file:
                messages.warning(request, 'Only JPG or JPEG allowed')
                return redirect('details')

            new_spl = SpecialItem(
                item_id=request.POST['special_food'],
                date=today,
                photo=spl_image_file,
                title=request.POST['heading'],
                description=request.POST['details'],
            )
            new_spl.save()
            messages.success(request, 'New Special item added')
            return redirect('details')

    return render(
        request, 'food_admin_1.html',
        {
            'today': today,
            'sales': sales,
            'food_item_names': food_item_names,
        }
    )

def list_order(request):
    if 'kpr_ticket' not in request.COOKIES:
        messages.warning(request, 'Please Login')
        return redirect('login')

    user = verify_kpr_ticket(request.COOKIES['kpr_ticket'])
    if not user:
        messages.warning(request, 'Please Login')
        return redirect('login')

    request_date = None
    request_date_sales = 00
    if 'request_date' in request.GET:
        request_date = request.GET['request_date']
        orders = Order.objects.filter(ordered_date=request_date).order_by('ordered_time')
        request_date_sales = Order.objects.filter(ordered_date=request_date).aggregate(total_price=Sum('total_cost'))

    else:
        orders = Order.objects.filter(ordered_date=today).order_by('ordered_time')

    order_details = []
    sales = Order.objects.filter(ordered_date=today).aggregate(total_price=Sum('total_cost'))
    paginator = Paginator(orders, 12)
    page = request.GET.get('page')
    try:
        orders = paginator.page(page)
    except PageNotAnInteger:
        orders = paginator.page(1)
    except EmptyPage:
        orders = paginator.page(paginator.num_pages)

    for order in orders:
        detail = {
            'id': order.id,
            'user_details': change_format(order.user_details),
            'items': change_format(order.items),
            'status': order.status,
            'total_cost': order.total_cost,
            'ordered_date': order.ordered_date,
            'ordered_time': order.ordered_time,
            'delivery_time': order.delivery_time,
        }
        order_details.append(detail)
    return render(request, 'orders_list.html', {
        'order_details': order_details,
        'orders': orders,
        'request_date_sales': request_date_sales,
        'sales': sales,
        'request_date':request_date,
    })

def change_format(items):
    char_list = ['[', '{', "'", '"', '}', ']']
    items = re.sub("|".join(char_list), "", items)
    items = items.replace('[', '')
    items = items.replace(']', '')
    items = items.split(', ')
    return items
