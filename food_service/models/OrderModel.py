from django.db import models

class Order(models.Model):

    login_id        =   models.IntegerField()
    user_details    =   models.CharField(max_length=2056)
    items           =   models.CharField(max_length=2056)
    status          =   models.CharField(max_length=50, default='confirmed')
    total_cost      =   models.IntegerField()
    ordered_date    =   models.DateField(null=True)
    ordered_time    =   models.TimeField(null=True)
    delivery_time   =   models.TimeField(null=True)

    objects = models.Manager()

    class Meta:
        db_table = "orders"
