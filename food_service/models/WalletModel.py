from django.db import models

class Wallet(models.Model):

    login_id    =   models.IntegerField(null=False, blank=False, unique=True)
    username    =   models.EmailField(unique=True, null=True)
    balance     =   models.IntegerField(default=0)
    created_at  =   models.DateTimeField(auto_now_add=True, null=True)
    updated_at  =   models.DateTimeField(auto_now=True, null=True)
    

    objects = models.Manager()

    class Meta:
        db_table = "wallets"
