from django.db import models
from food_service.models.FoodItemModel import FoodItem

class SpecialItem(models.Model):

    item        =   models.ForeignKey(FoodItem, on_delete=models.CASCADE, unique=False)
    photo       =   models.FileField(upload_to='food_service/special_items/', null=True, default='food_service/item_images/none1.jpg')
    date        =   models.DateField(null=True)
    title       =   models.CharField(max_length=128, null=True)
    description =   models.CharField(max_length=256, null=True)


    objects = models.Manager()

    class Meta:
        db_table = "special_items"
