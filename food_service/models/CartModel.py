from django.db import models
from food_service.models.FoodItemModel import FoodItem

class Cart(models.Model):

    login_id    =   models.IntegerField()
    item        =   models.ForeignKey(FoodItem, on_delete=models.CASCADE, unique=False)
    count       =   models.IntegerField()

    objects = models.Manager()

    class Meta:
        db_table = "carts"
