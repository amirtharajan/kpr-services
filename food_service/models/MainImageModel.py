from django.db import models
from django import forms

class MainImage(models.Model):

    main_photo      =   models.FileField(upload_to='food_service/slide/', null=True, default='food_service/item_images/none1.jpg')
    upload_at       =   models.DateTimeField(auto_now=True, null=True)

    objects = models.Manager()

    class Meta:
        db_table = "main_image"
