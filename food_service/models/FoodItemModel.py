from django.db import models
from django import forms

class FoodItem(models.Model):

    name            =   models.CharField(max_length=255, null=False, blank=False, default='item')
    photo           =   models.FileField(upload_to='food_service/item_images/', null=True, default='food_service/item_images/none1.jpg')
    is_available    =   models.BooleanField(default=True)
    available_count =   models.IntegerField(null=True, blank=True, default=0)
    prize           =   models.IntegerField(null=False, blank=False, default=0)
    food_family     =   models.CharField(max_length=40, null=False, blank=False, default='veg')
    type            =   models.CharField(max_length=100, null=True)
    available_from  =   models.TimeField(null=True)
    available_to    =   models.TimeField(null=True)
    created_at      =   models.DateTimeField(auto_now_add=True, null=True)
    updated_at      =   models.DateTimeField(auto_now=True, null=True)
    

    objects = models.Manager()

    class Meta:
        db_table = "food_items"

class NewItemFrom(forms.ModelForm):  
    class Meta:  
        model = FoodItem  
        fields = "__all__"
        exclude = ('created_at', 'updated_at')