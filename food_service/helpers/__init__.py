from django.conf import settings
from django.http import HttpResponseRedirect

from datetime import datetime
from food_service.models.WalletModel import Wallet

import jwt
from jwt import DecodeError

from io import BytesIO
from PIL import Image
from django.core.files.uploadedfile import InMemoryUploadedFile

JWT_SECRET_KEY = settings.JWT_SECRET_KEY
JWT_ALGORITHM = settings.JWT_ALGORITHM


def verify_kpr_ticket(kpr_ticket):
    if kpr_ticket:
        try:
            data = jwt.decode(kpr_ticket, JWT_SECRET_KEY, JWT_ALGORITHM)
            expire_date = datetime.strptime(data['expire'], '%Y-%m-%d %H:%M:%S.%f')
            if datetime.now() >= expire_date:
                return None
            return data

        except jwt.DecodeError:
            return None

    return None

def create_wallet(login_id, username):
    new_wallet = Wallet(
        login_id=login_id,
        username=username
    )
    new_wallet.save()
    return True

IMAGE_FILE_TYPES = ['jpg', 'png', 'jpeg', 'JPG', 'JPEG']
def image_handeller(image, name):
    check_file_type = str(image).split('.')
    if check_file_type[-1] not in IMAGE_FILE_TYPES:
        return False

    i = Image.open(image)
    i = i.convert('RGB')
    thumb_io = BytesIO()
    i.save(thumb_io, format='JPEG', quality=20)
    image_file = InMemoryUploadedFile(thumb_io, None, name + '.jpeg', None, thumb_io.tell(), None)
    return image_file

def image_handeller_high_quality(image, name):
    check_file_type = str(image).split('.')
    if check_file_type[-1] not in IMAGE_FILE_TYPES:
        return False

    i = Image.open(image)
    i = i.convert('RGB')
    thumb_io = BytesIO()
    i.save(thumb_io, format='JPEG', quality=70)
    image_file = InMemoryUploadedFile(thumb_io, None, name + '.jpeg', None, thumb_io.tell(), None)
    return image_file
