from django.urls import path, include
from food_service.controllers.admin.admin_controller import (
    food_admin,
    list_all,
    list_order,
)


urlpatterns = [
    path('', list_all, name='food_admin'),
    path('details/', food_admin, name='details'),
    path('details/orders-list', list_order, name='orders'),
]