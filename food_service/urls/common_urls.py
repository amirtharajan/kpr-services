
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

from food_service.controllers.common.common_controller import (
    list_items,
    make_order,
    my_order,
)


urlpatterns = [
    path('', list_items, name='food'),
    path('cart/', make_order, name='cart'),
    path('cart/order/', my_order, name='my_order')
]

urlpatterns = urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)